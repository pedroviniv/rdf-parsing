/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.websemantics.rdf.parsing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.parser.NxParser;

/**
 *
 * @author PedroArthur
 */
public class Loader {
    
    public static void main(String[] args) throws FileNotFoundException {
        
        InputStream inputStream = new FileInputStream("src/main/resources/ontologies/instance_types_en.nt");
        
        NxParser nxp = new NxParser();
        Iterator<Node[]> nodesIterator = nxp.parse(inputStream);
        
        Iterable<Node[]> nodesIterable = () -> nodesIterator;
        
        Stream<Node[]> nodesStream = StreamSupport
                .stream(nodesIterable.spliterator(), false);
        
        final String type = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
        
        final String ontology = "http://dbpedia.org/ontology/";
        
        long count = nodesStream
                .map(nodes -> nodes[2])
                .filter(node -> node.getLabel().startsWith(ontology))
                .distinct()
                .map(node -> node.getLabel().replaceAll(ontology, ""))
                .count();
        System.out.println(count);
        //.limit(100)
        //.sorted((l1, l2) -> l1.compareTo(l2))
        //.forEach(System.out::println);
                
        
    }
}
